﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Importamos
using Modelos;
using DB;

namespace GenerateDb
{
    class Program
    {
        static void Main(string[] args)
        {
            var Postulante01 = new Postulante()
            {
                Nombres="Carlos",
                ApellidoPaterno="Chavrz",
                ApellidoMaterno="Hinostroza",
                Sexo="Masculino",
                Celular="9763462566",
                CorreoElectronico="carlo@hotmail.com",
                DNI="78451976",
                Curriculum="Si"
            };
            //var Postulante02 = new Postulante()
            //{
            //    Nombres = "Tony",
            //    ApellidoPaterno = "Torres",
            //    ApellidoMaterno = "Vargas",
            //    Sexo = "Femenino",
            //    Celular = "9784512324",
            //    CorreoElectronico = "Hijomipona@jajaj.com",
            //    DNI = "78412563",
            //    Curriculum = "Si"
            //};
            //var Postulante03 = new Postulante()
            //{
            //    Nombres = "Yisus",
            //    ApellidoPaterno = "Chegne",
            //    ApellidoMaterno = "Chavez",
            //    Sexo = "Masculino",
            //    Celular = "9764518235",
            //    CorreoElectronico = "chegene@yahhoo.es",
            //    DNI = "12457896",
            //    Curriculum = "Si"
            //};

            var _context = new SGContratosContext();
            Console.WriteLine("Creando Base De Datos");

            _context.Postulantes.Add(Postulante01);
            //_context.Postulantes.Add(Postulante02);
            //_context.Postulantes.Add(Postulante03);

            _context.SaveChanges();

            Console.WriteLine("Base de datos creada con exito!!");
            Console.ReadLine();
        }
    }
}
