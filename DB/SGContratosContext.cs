﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Impportamos
using DB.Mapping;
using System.Data.Entity;
using Modelos;

namespace DB
{
  public  class SGContratosContext :DbContext
    {
        public SGContratosContext()
        {
            Database.SetInitializer<SGContratosContext>(null);
        }
        public DbSet<Postulante> Postulantes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PostulanteMapping());

        }
    }
}
