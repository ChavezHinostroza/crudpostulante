﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
   public interface InterfazPostulante
    {
        List<Postulante> All();
        void Store(Postulante postulante);
        Postulante Find(int id);
        void Update(Postulante producto);
        void Delete(int id);
        List<Postulante> ByQueryAll(string query);
    }
}
