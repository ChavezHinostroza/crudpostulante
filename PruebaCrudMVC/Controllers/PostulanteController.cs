﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//Imporatmaos
using Interfaces;
using Modelos;
using Validators.PostulanteValidators;

namespace PruebaCrudMVC.Controllers
{
    public class PostulanteController : Controller
    {
        //
        // GET: /Postulante/
        // ADD
        private InterfazPostulante repository;
        private PostulanteValidator validator;

        public PostulanteController(InterfazPostulante repository, PostulanteValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("CreatePostulante");
        }

        [HttpPost]
        public ActionResult Create(Postulante postulante)
        {

            if (validator.Pass(postulante))
            {
                repository.Store(postulante);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("Inicio", postulante);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("Edit", data);
        }

        [HttpPost]
        public ActionResult Edit(Postulante postulante)
        {
            repository.Update(postulante);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

    }
}
