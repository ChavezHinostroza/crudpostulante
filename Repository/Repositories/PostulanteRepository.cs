﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Importamos
using DB;
using Interfaces;
using Modelos;
using System.Data;

namespace Repository.Repositories
{
   public class PostulanteRepository: InterfazPostulante
    {
       //Add
        SGContratosContext entities;
        public PostulanteRepository(SGContratosContext entities)
        {
            this.entities = entities;
        }


        public List<Postulante> All()
        {
            var result = from p in entities.Postulantes select p;
            return result.ToList();
        }

        public void Store(Postulante postulante)
        {
            entities.Postulantes.Add(postulante);
            entities.SaveChanges();
        }


        public Postulante Find(int id)
        {
            var result = from p in entities.Postulantes where p.Id == id select p;
            return result.FirstOrDefault();
        }

        public void Update(Postulante postulante)
        {
            var result = (from p in entities.Postulantes where p.Id == postulante.Id select p).First();

            result.Nombres = postulante.Nombres;
            result.ApellidoPaterno = postulante.ApellidoPaterno;
            result.ApellidoMaterno = postulante.ApellidoMaterno;
            result.Sexo = postulante.Sexo;
            result.Celular = postulante.Celular;
            result.CorreoElectronico = postulante.CorreoElectronico;
            result.DNI = postulante.DNI;

            entities.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in entities.Postulantes where p.Id == id select p).First();
            entities.Postulantes.Remove(result);
            entities.SaveChanges();
        }


        public List<Postulante> ByQueryAll(string query)
        {
            var dbQuery = (from p in entities.Postulantes select p);

            if (!String.IsNullOrEmpty(query))
                dbQuery = dbQuery.Where(o => o.Nombres.Contains(query));

            return dbQuery.ToList();
        }
    }
}
