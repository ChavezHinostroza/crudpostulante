﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
  public  class Postulante
    {
        public int Id { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Sexo { get; set; }
        public string Celular { get; set; }
        public string CorreoElectronico { get; set; }
        public string DNI { get; set; }
        public string Curriculum { get; set; }
    }
}
